require('dotenv').config();
const express = require('express');
const PORT = process.env.PORT;
const mongoose = require('mongoose');
const app = require("./src/index");

app.use(express.json());
app.use(express.urlencoded({extended : false}))

const DB = process.env.DB;
 function connectdb(){
   mongoose.connect(DB,{
    useNewUrlParser:true,

  })
 }

app.listen(PORT,()=>{
  connectdb();
  console.info("Server ... started at PORT 3000")

})