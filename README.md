# Install the dependencies using : ```npm install```
# Run using : ```npm start```
# The api runs at ```http://localhost:3000/``` by default.

# We use gmail to send the mails.

## Email Microservice (https://gitlab.com/akhilnerella81/mailsendingservice ). By default it uses http://localhost:4000/

## Photo Upload Microservice(https://gitlab.com/akhilnerella81/photouploadservice).By default it uses http://localhost:5000/
# Add a .env file which contains :
``` 
PORT                    = Port at which the application should run. By default the application runs at port 3000,
DB                      = Mongodb url.I used mongodb atlas.That cluster url should be added here.
SEC_ACC_TOKEN               = Jwt key to sign the tokens.

```

# Routes
### POST ```/users/signup```
```
Request body should contain a JSON object of:
{
    "username" : "yourusername",
    "hash" : "yourpassword",
    "email" : "youremail",
}

```

### POST ```/users/login```
```
Request body should contain a JSON object of:
{
    "username" : "your_username",
    "password" : "your_password"
}

If login is done ,then the token created will be resulted.
```

### GET ```/auth/email_verify/:id```
```
User can activate account after signingup with the email sent to him.
```

### GET ```/users/forgot_password```
```
Request body should contain a JSON object of:
{
    "email" : your_email,
}
If the request is successful then the email will be sent where you can change the password.
```

### POST ```/auth/password_change/:id```
```
{
    "new_password" : your_new_password,
    "confirm_password" : enter the password again
}

```


### GET ```/users/:id```
```
User can get the detail of the other users if the id is given.
```

### PUT ```/users/:id```
```
User requires authorization to update details. The body the this request can contain this and others can also be added.
{
    "isPublic" : true/false,
    
}
```
## friends module

### GET ```/users/getall_sent_requests```
```
User gets all the friend requests he sent
```

### GET ```/users/getall_friend_requests```
```
User gets all the friend requests he received from other users
```

### GET ```/users/getall_friends```
```
User gets all his friend  he made.
```

### GET ```/users/:id```
```
User if he provides an id of the other user ,gets his username(..)
```
### PUT ```/users/:id```
```
User can  update his profile(like visiblity) given his id.
```

### POST ```/users/send-request```
```
User can send-request to any user with body.
{
    "requestfrnd":"friendusername"
}
```
### POST ```/users/accept-request/:id```
```
User can accept any request he received with the help of the parameter id

```
### POST ```/users/reject-request/:id```
```
User can reject any request he received with the help of the parameter id

```

### POST ```/users/remove-friend/:id```
```
User can remove any of his friends with the help of the parameter id 

```

### POST ```/users/upload-profilepic/:id```
```
Ths uses photoupload microservice

User can upload his profilepic via file in the formdata with key "image" 

```



## POST MODULE

### POST ```/users/posts/create```
```

User can create a post  with the body
{
   image : "uploadimagefile" (required)
   title: "yourtitle"          (required)
   caption: "yourcaption"
} 

```

### PUT ```/users/posts/update/:id```
```

User can update his post with the postID and also the field to be updated
{
   title: "yourtitle"          
   caption: "yourcaption"
} 

```


### GET  ```/users/posts/getpost/:id```
```
User can get the post with the postId .But the user can see if only the postCreater should be public account or a friend.

```
### GET  ```/users/posts/get_comments/:id```
```
User can see the comments of particular post with the postId but the post should be of public acc or an friends post.

```

### GET  ```/users/posts/deletepost/:id```
```

User can delete any of his posts with this get request.


```

### GET  ```/users/posts/share/:id```
```

User can share any of the public or friend's post in his account .Others can see that you shared from that user.


```

### POST  ```/users/posts/like/:id```
```

User can like any of the posts(friend's/public) 


```

### POST  ```/users/posts/unlike/:id```
```

User can remove the like which he liked before for a post.


```

## COMMENTS Module

### POST  ```/users/posts/comments/write/:id```
```
User can comment to a post with the postID and body
{
    "comment" :"your comment"
}

```
### PUT  ```/users/posts/comments/edit/:id```
```
User can edit any comment he commentedwith the commentID and body
{
    "comment" :"editcomment"
}

```
### GET  ```/users/posts/comments/delete/:id```
```
User can delete any comment he commented to a post with the commentID 

```

