const express = require('express');
const app = express();
const userRouter = require('./api/v1/routes/userRouter');
const authRouter = require('./api/v1/routes/authRouter');


app.use(express.json());
app.use(express.urlencoded({extended : false}))

app.get("/ping",function(req,res){
  return res.send({ status : 'PERFECT'});
})
app.use('/users', userRouter);
app.use("/auth",authRouter);

app.use((req, res, next)=>{
	const err = new Error("not found")
	err.status = 404
	next(err)
})

app.use((error, req, res, next)=>{
	return res.status(error.status || 500).json({
		error: {
			message: error.message
		}
	})
})

module.exports = app;


