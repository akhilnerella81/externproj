const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const formdata = require('form-data'); 
const axios = require('axios').default;
const User = require('../models/user')
const userService = require('../services/userService');
const mongoose = require('mongoose');
//const objectId = require('mongoose').Types.ObjectId;


module.exports.signup  = async function(req,res,next){
    try {
      console.log("signup");
      var salt = bcrypt.genSaltSync(10);
      var hashedpwd = bcrypt.hashSync(req.body.hash,salt);
      let data  = {
        username:req.body.username,
        email:req.body.email,
        hash : hashedpwd
        }
       // console.log("1");
        
        
        const signupAccessToken = jwt.sign(data,process.env.SEC_ACC_TOKEN);
        var postAddr = "http://localhost:3000/auth/email_verify/" + signupAccessToken;
        axios.post('htttp://localhost:4000/', {
          to: data.email,
          email_body :  postAddr
        })
        return res.send("Signedup Successfully,Please do the email verify");
  
    } catch (err) {
      next(err)
    }
  
  }

  module.exports.login = async function(req,res,next){
    try{
      //console.log("1");
      //console.log("login");
      if(!(req.body.username && req.body.password)) {
        var err = new Error("Username and passsword are required to login")
        err.status = 400
        throw err
      }
  
      const user = await User.findOne({username: req.body.username})
      const t =  bcrypt.compareSync(req.body.password, user.hash);
      if(t){
        // return res.status(200).send("Password Matched")
        const user = {
          name : req.body.username
        }
        const accessToken = jwt.sign(user,process.env.SEC_ACC_TOKEN)
       return res.json({accessToken:accessToken})
      }
      else{
      return res.status(400).send("Not matched")
      }
      
    }
    catch(err){
      next(err)
    }
  }

  module.exports.getusers = async function(req,res,next){
    try {
      const user = await User.findOne({_id:req.params.id})
      return res.send({
        id: user.id,
        username: user.username
      })
    } catch (err) {
        next(err);
    }
  }

  module.exports.updateProfile = async function(req,res,next){
    try {
      const user = await User.findOne({"_id":req.params.id})
      // console.log("fn username",user.username);
      
      if(req.user.name == user.username){
        if(isNaN(req.body.is_public)) {
          var err = new Error("body is required to update")
          err.status = 400
          throw err
        }
        // console.log("put");
         await User.findOneAndUpdate({_id:req.params.id},{$set : {is_public: req.body.is_public}})
  
        return res.send("Successfully updated.")}
      else{
        var err = new Error("Not authorised")
        err.status = 403
        throw err
      }
    } catch (err) {
        next(err);
    }
  }

  module.exports.email_verify = async function(req,res,next){
    try {
      console.log(req.params.id);
      const tokendata = jwt.verify(req.params.id,process.env.SEC_ACC_TOKEN)
      var user = new User(tokendata);
        await user.save();
        console.log("saved");
        res.send("Saved detailes successfully,Account Created")
    } catch (err) {
        next(err);
    }
  }

  module.exports.forgot_password = async function(req,res,next){
    try {
      console.log("forgot password");
      if(!(req.body.email)) {
        var err = new Error("Email required for forgot password")
        err.status = 400
        throw err
      }
  
      //to check if present in db or not const user = await User.findOne({email: req.body.email})
      let data  = {
        email:req.body.email
        }
        console.log("forgotpwdtoken");
        
        
        const forgotpwdToken = jwt.sign(data,process.env.SEC_ACC_TOKEN);
        var postAddr = "http://localhost:3000/auth/password_change/" + forgotpwdToken;
        axios.post('htttp://localhost:4000/', {
          to: data.email,
          email_body :  postAddr
        })
        return res.send("Go to the link for changing the password you forgot");
      
    } catch (err) {
        next(err);
      
    }
  }

  module.exports.password_change = async function(req,res,next){
    try {
      console.log(req.params.id);
      const tokendata = jwt.verify(req.params.id,process.env.SEC_ACC_TOKEN)
      //var user = new User(tokendata);
      const user = await User.findOne({email:tokendata.email})
      console.log(req.body.newpassword,req.body.confirmpassword);
      if(req.body.newpassword == req.body.confirmpassword){
        var salt = bcrypt.genSaltSync(10);
        var hashedpwd = bcrypt.hashSync(req.body.newpassword,salt);
        console.log(hashedpwd,tokendata.email);
        await User.findOneAndUpdate({email:tokendata.email},{$set : {hash: hashedpwd}})
        return res.send("Password updated succefully")
      }else{
        return res.send("password not matching")
      }
        
    } catch (err) {
        next(err);
    }
  
  }

  module.exports.send_request = async function(req,res,next){
    try{

      const requestuser = await userService.findbyUsername(req.body.requestfrnd);
      const user =  await userService.findbyUsername(req.user.name);

      if(req.user.name==req.body.requestfrnd){
        return res.status(400).json({
        message: "You cannot send request to yourself"
        })
       }


      if(!requestuser){
        return res.status(400).json({
          message: "The user you requested doesnt exist"
        })
      }
 
      if(await userService.findRequest(user.id,requestuser.id)){
        return res.status(200).json({
          message : "you alreaady sent him the request."
        })
      }

      if(await userService.checkFriend(user.id,requestuser.id)){
        return res.status(200).json({
          message : "You both are already friends."
        })

      }

      requestuser.requestReceived.push(user.id);
      await requestuser.save();
      user.requestSent.push(requestuser.id);
      await user.save();

      return res.status(200).json({
        messsage:"Friend Request Sent Succcessfuly"}
        );

    }
    
    catch(error){
      next(error)
    }
  }


module.exports.reject_request = async function(req,res,next){
  try {
     if(!mongoose.Types.ObjectId.isValid(req.params.id)){
      return res.status(400).json({
        message: "The request to be rejected does not exist"
      })
      }
    const myuser =  await userService.findbyUsername(req.user.name);
    const otheruser = await User.findById(req.params.id);

    if(!(await userService.findRequest(otheruser.id,myuser.id))){
      return res.status(200).json({
        message : "The request received not found"
      })
    }

    myuser.requestReceived.pull(otheruser.id);
    otheruser.requestSent.pull(myuser.id);
    await myuser.save();
    await otheruser.save();
    return res.send("Rejected FriendRequest Successfully!!");
  } catch (error) {
     next(error)  
  }
}

module.exports.accept_request = async function(req,res,next){
  try 
    {
      if(!mongoose.Types.ObjectId.isValid(req.params.id)){
        return res.status(400).json({
          message: "The request to be rejected does not exist"
        })
        }
    const myuser =  await userService.findbyUsername(req.user.name);
    const otheruser = await User.findById(req.params.id);

    if(!(await userService.findRequest(otheruser.id,myuser.id))){
      return res.status(200).json({
        message : "The request received not found"
      })
    }

    myuser.requestReceived.pull(otheruser.id);
    otheruser.requestSent.pull(myuser.id);
    myuser.friendsArray.push(otheruser.id);
    otheruser.friendsArray.push(myuser.id);

   await myuser.save();
   await otheruser.save();

    return res.send("Added Friend Successfully!!");

    }
    catch (error) 
    {
      next(error)
    }
  }


  module.exports.remove_friend = async function(req,res,next){
    try {
      if(!mongoose.Types.ObjectId.isValid(req.params.id)){
        return res.status(400).json({
          message: "The request to be rejected does not exist"
        })
        }
        const myuser =  await userService.findbyUsername(req.user.name);
        const otheruser = await User.findById(req.params.id);

        if(!(await userService.checkFriend(myuser.id,otheruser.id))){
          return res.status(200).json({
            message : "You both are not friends"
          })
  
        }

      
      myuser.friendsArray.pull(otheruser.id);
      otheruser.friendsArray.pull(myuser.id);
      await myuser.save();
      await otheruser.save();

    return res.send("Removed Friend Successfully!!");

    } catch (error) {
        next(error)
    }
  }

  module.exports.getall_sent_requests= async function(req,res,next){
    try {
      
      const myuser =  await userService.findbyUsername(req.user.name);
      const users = await User.findById(myuser.id).populate({path: "requestSent", select: "username"});

      return res.status(200).send(users.requestSent);
    
    } catch (error) {
        
    }
  }


  module.exports.getall_friend_requests= async function(req,res,next){
    try {
      const myuser =  await userService.findbyUsername(req.user.name);
      const users = await User.findById(myuser.id).populate({path: "requestReceived", select: "username"});
      return res.send(users.requestReceived);
    
    } catch (error) {
       next(error) 
    }
  }


  module.exports.getall_friends= async function(req,res,next){
    try {
      const myuser =  await userService.findbyUsername(req.user.name);
      const users = await User.findById(myuser.id).populate({path: "friendsArray", select: "username"});

      return res.send(users.friendsArray);
    
    } catch (error) {
        next(error)
    }
  }

  module.exports.upload_pic = async function(req,res,next){
    try{
      const myuser =  await userService.findbyUsername(req.user.name);
      var bodyFormData = new formdata();
      bodyFormData.append('image', req.file.buffer, req.file.originalname);

      var response = await axios({
        method: "post",
        url: 'http://localhost:5000/upload-image',
        data: bodyFormData,
        headers: { 'Content-Type': `multipart/form-data; boundary=${bodyFormData._boundary}`},
      })

      myuser.profilepicture = response.data.imgsrc;
      await myuser.save();
      return res.status(200).send("Uploaded Successfully");

    }
    catch(error){
        next(error)
    }
  }