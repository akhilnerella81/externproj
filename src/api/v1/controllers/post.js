//const post = require('../models/post')
const Post = require('../models/post')
const User = require('../models/user')
const Comment = require('../models/comments')
const mongoose = require('mongoose')
const formdata = require('form-data'); 
const axios = require('axios').default;
const userService = require('../services/userService');

module.exports.create_post = async function(req,res,next){
    try {
        if(!req.body.title || !req.file ){
            return res.status(400).json({
                message: "Title and imagefile is required"
              })
        }
        var bodyFormData = new formdata();
        bodyFormData.append('image', req.file.buffer, req.file.originalname);
        const user = await User.findOne({username:req.user.name});

         var response = await axios({
           method: "post",
           url: 'http://localhost:5000/upload-image',
           data: bodyFormData,
           headers: { 'Content-Type': `multipart/form-data; boundary=${bodyFormData._boundary}`},
         })
 
        var postbody =  
        {
            title: req.body.title,
            caption: req.body.caption,
            userid: user.id,
            imageurl:response.data.imgsrc
            
        }
        var newpost = new Post(postbody);
        await newpost.save();
        user.postArray.push(newpost.id);
        await user.save();
        return res.send("Post created successfully")
    } catch (error) {
        next(error)
    }
}



module.exports.update_post = async function(req,res,next){
    try {
        if(!mongoose.Types.ObjectId.isValid(req.params.id)){
            return res.status(400).json({
              message: "PostID invalid"
            })
            }
        var mypost =await Post.findById(req.params.id);
        if(!mypost){
            return res.status(400).json({
                message: "Post not available"
              })
        }
        const myuser =  await userService.findbyUsername(req.user.name);

        if(myuser.id!=mypost.userid){
            return res.status(400).json({
                message: "You dont have access to update post"
              })
        }
        if(req.body.title){
            mypost.title = req.body.title    
        }
        if(req.body.caption){
            mypost.caption = req.body.caption
        }
        await mypost.save();
        return res.send("Post updated Successfully");
    } catch (error) {
        next(error)
    }
}

module.exports.get_post = async function(req,res,next){
    try {
        if(!mongoose.Types.ObjectId.isValid(req.params.id)){
            return res.status(400).json({
              message: "Post ID invalid"
            })
            }
        var mypost =await Post.findById(req.params.id);
        const postCreator =  await User.findById(mypost.userid);
        const myuser =  await userService.findbyUsername(req.user.name);
        if(postCreator.is_public || await userService.checkFriend(myuser.id,postCreator.id) || myuser.id == postCreator.id){
            return res.send(mypost);
        }
        else{
            return res.status(200).json({
                message:"To see the post either post shuld be public or you should be friends"
            })
        }
        
       
    } catch (error) {
        next(error)
    }
}

module.exports.delete_post = async function(req,res,next){
    try {
        if(!mongoose.Types.ObjectId.isValid(req.params.id)){
            return res.status(400).json({
              message: "Post ID invalid"
            })
            }

        const user =  await userService.findbyUsername(req.user.name);
        var mypost =await Post.findById(req.params.id);

        if(user.id!=mypost.userid){
                return res.status(400).json({
                    message: "You dont have access to delete post"
                  })
            }

        await Post.deleteOne({"id":req.params.id});
        user.postArray.pull(req.params.id);
        await user.save();
        return res.send("Post deleted Successfully");
    } catch (error) {
        next(error)
    }
}


module.exports.get_comments = async function(req,res,next){
    try {
        if(!mongoose.Types.ObjectId.isValid(req.params.id)){
            return res.status(400).json({
              message: "Post ID invalid"
            })
            }
        var mypost =await Post.findById(req.params.id);
        const postCreator =  await User.findById(mypost.userid);
        const myuser =  await userService.findbyUsername(req.user.name);
        if(postCreator.is_public || await userService.checkFriend(myuser.id,postCreator.id) || myuser.id == postCreator.id){
            const comments = await Post.findById(req.params.id).populate({path:"commentArray",select:"commentData"})
            return res.send(comments.commentArray);
        }
        else{
            return res.status(200).json({
            message:"To see the post either post shuld be public or you should be friends"
                })
            }
        

    } catch (error) {
        next(error)
    }
}

module.exports.share_post = async function(req,res,next){
    try {
        if(!mongoose.Types.ObjectId.isValid(req.params.id)){
            return res.status(400).json({
              message: "Post ID invalid"
            })
            }
        var mypost =await Post.findById(req.params.id);
        const postCreator =  await User.findById(mypost.userid);
        const user =  await userService.findbyUsername(req.user.name);
        if(!(postCreator.is_public) && !(await userService.checkFriend(user.id,postCreator.id)) && !(user.id == postCreator.id)){
            return res.status(200).json({
                message:"To share the post either post shuld be public or you should be friends"
                })
        
        }
        
        var this_post = {
            title:mypost.title,
            caption:mypost.caption,
            imageurl:mypost.imageurl,
            sharedid:mypost.userid,
            userid:user.id
        }

        var newPost = new Post(this_post);

        await newPost.save();
        user.postArray.push(newPost.id);

        await user.save();
        return res.send("Shared post successfully");

    } catch (error) {
        next(error);
    }
}


module.exports.like_post = async function(req,res,next){
    try {
        if(!mongoose.Types.ObjectId.isValid(req.params.id)){
            return res.status(400).json({
              message: "Post ID invalid"
            })
            }

        var mypost =await Post.findById(req.params.id);
        const postCreator =  await User.findById(mypost.userid);
        const user =  await userService.findbyUsername(req.user.name);

        if(!(postCreator.is_public) && !(await userService.checkFriend(user.id,postCreator.id)) && !(user.id == postCreator.id)){
            return res.status(200).json({
                message:"To like the post either post shuld be public or you should be friends"
                })
        }

        if(mypost.likes.includes(user.id)){
            
            return res.status(200).send("You already liked the photo");
       }
        
        mypost.likes.push(user.id);
        await mypost.save();
        return res.send("liked post successfully");

    } catch (error) {
        next(error)
    }
}



module.exports.unlike_post = async function(req,res,next){
    try {
        if(!mongoose.Types.ObjectId.isValid(req.params.id)){
            return res.status(400).json({
              message: "Post ID invalid"
            })
            }
        
        var mypost =await Post.findById(req.params.id);
        const postCreator =  await User.findById(mypost.userid);
        const user = await User.findOne({username:req.user.name});

        if(!(postCreator.is_public) && !(await userService.checkFriend(user.id,postCreator.id)) && !(user.id == postCreator.id)){
            return res.status(200).json({
                message:"To unlike the post either post shuld be public or you should be friends"
                })
        }

        if(mypost.likes.includes(user.id)){
            mypost.likes.pull(user.id);
            await mypost.save();
            return res.send("unliked post successfully");
        }
        else{
            return res.status(200).json({
                message:"You should have liked to unlike the post"
            })
        }
    } catch (error) {
        next(error)
    }
}

module.exports.all_posts = async function(req,res,next){
    try {
    
        const allposts= await Post.find();
        return res.send(allposts);
    } catch (error) {
        next(error);
    }
}
