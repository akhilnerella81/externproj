const Post = require('../models/post')
const User = require('../models/user')
const Comment = require('../models/comments')
const userService = require('../services/userService');
const mongoose = require('mongoose')

module.exports.write_comment = async function(req,res,next){
    try {
        
        var this_post = await Post.findById(req.params.id);
        if(!this_post){
            return res.status(400).json({
                message:"Such post doesnt exist"
            })        }
        const postCreator =  await User.findById(this_post.userid);
        const user = await User.findOne({username:req.user.name});

        if(!(postCreator.is_public) && !(await userService.checkFriend(user.id,postCreator.id)) && !(user.id == postCreator.id)){
            return res.status(200).json({
                message:"To write comment for the post either post shuld be public or you should be friends"
                })
        }


        var commentBody= {
            userid : user.id,
            commentData : req.body.comment,
            postid: req.params.id
        }

        var comment1 = new Comment(commentBody);
        await comment1.save();
        this_post.commentArray.push(comment1.id);
        await this_post.save();
        return res.send("comment written to the post");
    } catch (error) {
        next(error)
    }
}

module.exports.edit_comment = async function(req,res,next){
    try {
        if(!mongoose.Types.ObjectId.isValid(req.params.id)){
            return res.status(400).json({
              message: "The comment does not exist"
            })
            }
        var this_comment = await Comment.findById(req.params.id);
        if(!this_comment){
            return res.status(400).json({
                message:"Such comment doesnt exist"
            })        }
        
        var user = await User.findOne({username:req.user.name});
        if(!(this_comment.userid == user.id)){
            return res.status(400).json({
                message:"Not authorised to edit comment"
            })        
        }
        this_comment.commentData = req.body.comment;
        await this_comment.save();
        return res.send("Comment Edited Succesfully");
    } catch (error) {
        next(error)

    }
}

module.exports.delete_comment = async function(req,res,next){
    try {
        if(!mongoose.Types.ObjectId.isValid(req.params.id)){
            return res.status(400).json({
              message: "The comment does not exist"
            })
            }
        var this_comment = await Comment.findById(req.params.id);
        var user = await User.findOne({username:req.user.name});
        if(!this_comment){
            return res.status(400).json({
                message:"Such comment doesnt exist"
            })        }
        if(!(this_comment.userid == user.id)){
            return res.status(400).json({
                message:"Not authorised to delete comment"
            })
        } 

        var this_post = await Post.findById(this_comment.postid);
            this_post.commentArray.pull(this_comment.id);
            await Comment.findByIdAndDelete(req.params.id);
            await this_post.save();
        return res.send("Deleted Comment Successfully")
    } catch (error) {
        next(error)

    }
}