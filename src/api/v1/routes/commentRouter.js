const express = require('express');
const router = express.Router();
const middlewares = require('../middlewares/user');
const controller = require('../controllers/comments');

router.post("/write/:id",middlewares.authenticateToken,controller.write_comment)

router.put("/edit/:id",middlewares.authenticateToken,controller.edit_comment)

router.get("/delete/:id",middlewares.authenticateToken,controller.delete_comment)



//router.get("/",middlewares.authenticateToken,controller.get_comments)








module.exports = router;
