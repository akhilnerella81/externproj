
const express = require("express");
const router = express.Router();
const multer = require('multer')();
// const userRouter = require('./api/v1/routes/userRouter.js');
const middlewares = require('../middlewares/user');
const controller = require('../controllers/user')
const postRouter = require('./postRouter');

router.use('/posts',postRouter);

router.post("/signup", controller.signup)
router.post("/login",controller.login)
router.post("/forgot_password",controller.forgot_password)
router.get("/getall_sent_requests",middlewares.authenticateToken,controller.getall_sent_requests)
router.get("/getall_friend_requests",middlewares.authenticateToken,controller.getall_friend_requests)
router.get("/getall_friends",middlewares.authenticateToken,controller.getall_friends)


router.get("/:id",controller.getusers)
router.put("/:id",middlewares.authenticateToken,controller.updateProfile)

router.post("/send-request",middlewares.authenticateToken,controller.send_request)
router.post("/accept-request/:id",middlewares.authenticateToken,controller.accept_request)
router.post("/reject-request/:id",middlewares.authenticateToken,controller.reject_request)
router.post("/remove-friend/:id",middlewares.authenticateToken,controller.remove_friend)

router.post("/upload-profilepic",middlewares.authenticateToken, multer.single("image") ,controller.upload_pic)



module.exports = router