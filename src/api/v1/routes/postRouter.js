const express = require("express");
const router = express.Router();
const middlewares = require('../middlewares/user');
const controller = require('../controllers/post')
const multer = require('multer')();
const commentRouter = require('./commentRouter');


router.use("/comments",commentRouter);
router.post("/create",middlewares.authenticateToken,multer.single("image"),controller.create_post)


router.put("/update/:id",middlewares.authenticateToken,controller.update_post)

router.get("/getpost/:id",middlewares.authenticateToken,controller.get_post)

router.get("/get_comments/:id",middlewares.authenticateToken,controller.get_comments)


router.get("/deletepost/:id",middlewares.authenticateToken,controller.delete_post)

router.get("/share/:id",middlewares.authenticateToken,controller.share_post)

router.post("/like/:id",middlewares.authenticateToken,controller.like_post)

router.post("/unlike/:id",middlewares.authenticateToken,controller.unlike_post)


router.get("/allposts",middlewares.authenticateToken,controller.all_posts)











module.exports = router;