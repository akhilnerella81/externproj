const express = require("express");
const router = express.Router();
const middlewares = require('../middlewares/user');
const controller = require('../controllers/user')


router.get("/email_verify/:id",controller.email_verify)
router.post("/password_change/:id",controller.password_change)


module.exports = router