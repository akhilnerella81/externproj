const mongoose = require('mongoose');

const commentDetails = new mongoose.Schema({
    commentData:{
        type:String,
        required:true
    },

    userid: {
        type: mongoose.Types.ObjectId,
         ref: 'User'
    },

    postid: {
        type: mongoose.Types.ObjectId,
         ref: 'Post'
    }



    


}, { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' }})

module.exports = mongoose.model('Comments',commentDetails);