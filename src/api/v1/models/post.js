const mongoose = require('mongoose');
const User = require('./user');
const Comments = require('./comments');


const postDetails = new mongoose.Schema({
    title:{
        type:String,
        required:true
    },
    caption:{
        type:String
    },
    imageurl:{
        type:String,
        required:true
    },
    
    likes: [{ type: mongoose.Types.ObjectId, ref: 'User' }],

    hashtags:[String],

    userid: {
        type: mongoose.Types.ObjectId,
         ref: 'User'
    },

    sharedid:{
        type: mongoose.Types.ObjectId,
        ref:'User',
        default : null
    },
    commentArray : [{type: mongoose.Types.ObjectId,ref: 'Comments'}]
 }, { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' }} )

module.exports = mongoose.model("Post",postDetails);
