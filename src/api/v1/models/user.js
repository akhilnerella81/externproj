const mongoose = require('mongoose');

const userDetails = new mongoose.Schema({
    username :{
      type:String,
      required:true,
      unique:true
    },
    hash : {
      type:String,
      required:true
  
    },
    email : {
      type:String,
      index:true,
      sparse:true,
      required:true,
       unique:true
    },
    email_verified :{
      type:Boolean,
      default : false
  
    },
  
    is_public :{
      type:Boolean,
      default:true
  
    },
    profilepicture:{
      type:String
    },

    friendsArray: [{ type: mongoose.Types.ObjectId, ref: 'User' }],

    requestSent: [{type: mongoose.Types.ObjectId, ref: 'User'}],

    requestReceived : [{type: mongoose.Types.ObjectId, ref: 'User'}],

    postArray :[{type:mongoose.Types.ObjectId,ref:'Post'}]



  },{ timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } })




  module.exports = mongoose.model("User",userDetails);

  