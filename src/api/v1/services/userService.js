const Post = require('../models/post')
const User = require('../models/user')
const Comment = require('../models/comments')



module.exports.findbyUsername = async (query)=>{
   
    const Check = await User.findOne({'username':query});
    return Check;
    
}

module.exports.findRequest = async (query1,query2)=>{
    const user = await User.findById(query1);
    const check = user.requestSent.includes(query2);
    return check;

}

module.exports.checkFriend = async (query1,query2)=>{
    const user = await User.findById(query1);
    const friendstatus = user.friendsArray.includes(query2);
    return friendstatus;
    
}

