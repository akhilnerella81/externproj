
const jwt = require('jsonwebtoken');

module.exports.authenticateToken = (req,res,next)=>{
    //console.log(req.params);
    const authHeader = req.headers['authorization'];
    const token = authHeader && authHeader.split(' ')[1]
    if(token==null) return res.sendStatus(401)
    const details = jwt.verify(token,process.env.SEC_ACC_TOKEN)
    // console.log(details.name);
    req.user = details
    
    next()
  }
  